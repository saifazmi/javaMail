package resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaMail
 * @date : 11/11/15
 */

/**
 * Represents a file storing client properties
 */
public class ClientProperties {

    private static final Logger log = Logger.getLogger(ClientProperties.class.getName());
    private final String FILE_NAME = "client.properties";
    private final File PROPERTIES_FILE = new File("./resources/" + this.FILE_NAME);

    private final String storeProtocolKey = "mail.store.protocol";
    private final String smtpHostKey = "mail.smtp.host";
    private final String usernameKey = "mail.user";
    private final String passwordKey = "mail.password";

    private String storeProtocol;
    private String smtpHost;
    private String username;
    private String password;

    /**
     * For reading the properties file
     */
    public ClientProperties() {

        readProperties();
    }

    /**
     * Creates a properties file
     * @param username email address of the user.
     * @param password password of the user.
     */
    public ClientProperties(String username, String password) {

        this.storeProtocol = "imaps";
        this.smtpHost = "smtp.gmail.com";
        this.username = username;
        this.password = password;

        writeProperties();
    }

    /**
     * Writes the specified data to the properties file.
     */
    private void writeProperties() {

        try (FileOutputStream fileOut = new FileOutputStream(this.PROPERTIES_FILE);) {

            Properties clientProperties = new Properties();
            clientProperties.setProperty(this.storeProtocolKey, this.storeProtocol);
            clientProperties.setProperty(this.smtpHostKey, this.smtpHost);
            clientProperties.setProperty(this.usernameKey, this.username);
            clientProperties.setProperty(this.passwordKey, this.password);

            clientProperties.store(fileOut, "Clients Email login credentials");

        } catch (IOException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }
    }

    /**
     * Reads and stores the data from the proprieties file
     */
    private void readProperties() {

        try (InputStream inputStream = new FileInputStream(this.PROPERTIES_FILE)) {

            Properties clientProperties = new Properties();
            clientProperties.load(inputStream);
            this.storeProtocol = clientProperties.getProperty(this.storeProtocolKey);
            this.smtpHost = clientProperties.getProperty(this.smtpHostKey);
            this.username = clientProperties.getProperty(this.usernameKey);
            this.password = clientProperties.getProperty(this.passwordKey);

        } catch (IOException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }
    }

    // Getter function to support the read functionality.
    public String getStoreProtocol() {
        return storeProtocol;
    }

    public String getSmtpHost() {
        return smtpHost;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
