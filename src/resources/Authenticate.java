package resources;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaMail
 * @date : 12/11/15
 */
public class Authenticate {

    private static final Logger log = Logger.getLogger(Authenticate.class.getName());

    private ClientProperties loginCredentials;
    public Authenticate() {

    }

    public Authenticate(String username, String password) {
        this.loginCredentials = new ClientProperties(username, password);
    }

    public boolean validLogin(final String username, final String password) {
        this.loginCredentials =  new ClientProperties();
        return (username.equals(loginCredentials.getUsername())
                && password.equals(loginCredentials.getPassword()));
    }

}
