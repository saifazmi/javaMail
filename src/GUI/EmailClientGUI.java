/*
 * Created by JFormDesigner on Wed Nov 11 21:03:46 GMT 2015
 */

package GUI;

import mail.Search;
import mail.Send;
import mail.folders.Inbox;
import resources.Authenticate;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Flags;
import javax.mail.Message;
import javax.swing.*;
import javax.swing.event.*;

import static mail.Send.createRecepientList;

/**
 * @author Saifullah Azmi
 */
public class EmailClientGUI extends JFrame {

    private static final Logger log = Logger.getLogger(EmailClientGUI.class.getName());
    private ArrayList<File> attachedFiles;

    public EmailClientGUI() {
        initComponents();
        initLogin();
    }

    private void initLogin() {
        JPanel panel = new JPanel(new GridLayout());
        JLabel userLabel = new JLabel("UserName: ");
        JTextField userField = new JTextField();
        JLabel passLabel = new JLabel("Password: ");
        JPasswordField pwdField = new JPasswordField(20);
        panel.add(userLabel);
        panel.add(userField);
        panel.add(passLabel);
        panel.add(pwdField);

        JOptionPane login = new JOptionPane();
        int result = login.showConfirmDialog(null, panel, "Enter Login Details", JOptionPane.OK_CANCEL_OPTION);

        if (result == 0) {
            Authenticate auth = new Authenticate();
            if (auth.validLogin(userField.getText(), new String(pwdField.getPassword()))) {
                JOptionPane.showMessageDialog(null, "Credentials Valid!!!");
                initInbox(new Inbox());
                initSendMessage();
                initSearch(new Search());
            } else {
                JOptionPane.showMessageDialog(null, "Username/Password incorrect");
                initLogin();
            }
        } else {
            System.exit(0);
        }
    }

    private void initInbox(final Inbox inbox) {

        inbox.open();
        emailList.setModel(inbox.getSubjectList());
        emailList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int listIndex = emailList.getSelectedIndex();
                tags.setText(inbox.getMessageTags(listIndex));
                fromEmailAddress.setText(inbox.getMessageFrom(listIndex));
                messageDate.setText(inbox.getMessageReceiveDate(listIndex));
                emailMessage.setText(inbox.getMessageList().get(listIndex));

                inbox.setMessageFlags(listIndex, Flags.Flag.SEEN, true);
            }
        });

    }

    private void initSendMessage() {
        attachmentButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.showOpenDialog(null);
                File file = fileChooser.getSelectedFile();
                attachedFiles = new ArrayList<>(Arrays.asList(file));
            }
        });

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Send sendMessage = new Send(
                        createRecepientList(toAddresses.getText()),
                        createRecepientList(ccAddresses.getText()),
                        newSubject.getText(),
                        messageBody.getText(),
                        attachedFiles
                );

                JOptionPane.showMessageDialog(null, "Message Sent");

                toAddresses.setText("");
                ccAddresses.setText("");
                newSubject.setText("");
                messageBody.setText("");
            }
        });
    }

    private void initSearch(final Search search) {
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                search.searchFolder(searchArea.getText().toLowerCase());
                resultMsgSubjects.setModel(search.getSubjectList());
                resultMsgSubjects.addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        int listIndex = resultMsgSubjects.getSelectedIndex();
                        resultMsgBody.setText(search.getMessageList().get(listIndex));
                    }
                });
            }
        });
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Saifullah Azmi
        OptionsPane = new JTabbedPane();
        InboxView = new JPanel();
        subjectScroll = new JScrollPane();
        emailList = new JList();
        MessagePanel = new JPanel();
        MessaageDetails = new JPanel();
        fromLabel = new JLabel();
        fromEmailAddress = new JTextPane();
        dateLabel = new JLabel();
        messageDate = new JTextPane();
        tagsLabel = new JLabel();
        tags = new JTextPane();
        messageScroll = new JScrollPane();
        emailMessage = new JTextArea();
        SendView = new JPanel();
        NewEmailPane = new JPanel();
        CompseDetailsPane = new JPanel();
        toLabel = new JLabel();
        toAddScroll = new JScrollPane();
        toAddresses = new JTextArea();
        ccLabel = new JLabel();
        ccAddScroll = new JScrollPane();
        ccAddresses = new JTextArea();
        subjectLabel = new JLabel();
        newSubjectScroll = new JScrollPane();
        newSubject = new JTextArea();
        sendButton = new JButton();
        attachmentButton = new JButton();
        bodyScroll = new JScrollPane();
        messageBody = new JEditorPane();
        SearchView = new JPanel();
        panel1 = new JPanel();
        scrollPane1 = new JScrollPane();
        searchArea = new JTextArea();
        searchButton = new JButton();
        scrollPane2 = new JScrollPane();
        resultMsgSubjects = new JList();
        scrollPane3 = new JScrollPane();
        resultMsgBody = new JTextArea();

        //======== this ========
        setTitle("Email Client");
        Container contentPane = getContentPane();
        contentPane.setLayout(new GridLayout());

        //======== OptionsPane ========
        {

            //======== InboxView ========
            {

                // JFormDesigner evaluation mark
                InboxView.setBorder(new javax.swing.border.CompoundBorder(
                    new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                        "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                        javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                        java.awt.Color.red), InboxView.getBorder())); InboxView.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

                InboxView.setLayout(new BorderLayout());

                //======== subjectScroll ========
                {
                    subjectScroll.setViewportView(emailList);
                }
                InboxView.add(subjectScroll, BorderLayout.LINE_START);

                //======== MessagePanel ========
                {
                    MessagePanel.setLayout(new BorderLayout());

                    //======== MessaageDetails ========
                    {
                        MessaageDetails.setLayout(null);

                        //---- fromLabel ----
                        fromLabel.setText("From: ");
                        MessaageDetails.add(fromLabel);
                        fromLabel.setBounds(new Rectangle(new Point(20, 5), fromLabel.getPreferredSize()));

                        //---- fromEmailAddress ----
                        fromEmailAddress.setEditable(false);
                        MessaageDetails.add(fromEmailAddress);
                        fromEmailAddress.setBounds(65, 5, 605, 20);

                        //---- dateLabel ----
                        dateLabel.setText("Date: ");
                        MessaageDetails.add(dateLabel);
                        dateLabel.setBounds(new Rectangle(new Point(20, 35), dateLabel.getPreferredSize()));

                        //---- messageDate ----
                        messageDate.setEditable(false);
                        MessaageDetails.add(messageDate);
                        messageDate.setBounds(65, 35, 315, 20);

                        //---- tagsLabel ----
                        tagsLabel.setText("Tags: ");
                        MessaageDetails.add(tagsLabel);
                        tagsLabel.setBounds(20, 55, tagsLabel.getPreferredSize().width, 40);
                        MessaageDetails.add(tags);
                        tags.setBounds(65, 65, 760, 20);

                        { // compute preferred size
                            Dimension preferredSize = new Dimension();
                            for(int i = 0; i < MessaageDetails.getComponentCount(); i++) {
                                Rectangle bounds = MessaageDetails.getComponent(i).getBounds();
                                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                            }
                            Insets insets = MessaageDetails.getInsets();
                            preferredSize.width += insets.right;
                            preferredSize.height += insets.bottom;
                            MessaageDetails.setMinimumSize(preferredSize);
                            MessaageDetails.setPreferredSize(preferredSize);
                        }
                    }
                    MessagePanel.add(MessaageDetails, BorderLayout.NORTH);

                    //======== messageScroll ========
                    {

                        //---- emailMessage ----
                        emailMessage.setEditable(false);
                        emailMessage.setLineWrap(true);
                        emailMessage.setWrapStyleWord(true);
                        messageScroll.setViewportView(emailMessage);
                    }
                    MessagePanel.add(messageScroll, BorderLayout.CENTER);
                }
                InboxView.add(MessagePanel, BorderLayout.CENTER);
            }
            OptionsPane.addTab("Inbox", InboxView);

            //======== SendView ========
            {
                SendView.setLayout(new GridLayout());

                //======== NewEmailPane ========
                {
                    NewEmailPane.setLayout(new BorderLayout());

                    //======== CompseDetailsPane ========
                    {
                        CompseDetailsPane.setLayout(null);

                        //---- toLabel ----
                        toLabel.setText("To:");
                        CompseDetailsPane.add(toLabel);
                        toLabel.setBounds(new Rectangle(new Point(20, 15), toLabel.getPreferredSize()));

                        //======== toAddScroll ========
                        {

                            //---- toAddresses ----
                            toAddresses.setLineWrap(true);
                            toAddresses.setWrapStyleWord(true);
                            toAddScroll.setViewportView(toAddresses);
                        }
                        CompseDetailsPane.add(toAddScroll);
                        toAddScroll.setBounds(50, 5, 745, 35);

                        //---- ccLabel ----
                        ccLabel.setText("CC:");
                        CompseDetailsPane.add(ccLabel);
                        ccLabel.setBounds(new Rectangle(new Point(20, 60), ccLabel.getPreferredSize()));

                        //======== ccAddScroll ========
                        {

                            //---- ccAddresses ----
                            ccAddresses.setLineWrap(true);
                            ccAddresses.setWrapStyleWord(true);
                            ccAddScroll.setViewportView(ccAddresses);
                        }
                        CompseDetailsPane.add(ccAddScroll);
                        ccAddScroll.setBounds(50, 50, 745, 35);

                        //---- subjectLabel ----
                        subjectLabel.setText("Subject:");
                        CompseDetailsPane.add(subjectLabel);
                        subjectLabel.setBounds(20, 90, subjectLabel.getPreferredSize().width, 60);

                        //======== newSubjectScroll ========
                        {

                            //---- newSubject ----
                            newSubject.setLineWrap(true);
                            newSubject.setWrapStyleWord(true);
                            newSubjectScroll.setViewportView(newSubject);
                        }
                        CompseDetailsPane.add(newSubjectScroll);
                        newSubjectScroll.setBounds(80, 100, 715, 35);

                        //---- sendButton ----
                        sendButton.setText("SEND");
                        CompseDetailsPane.add(sendButton);
                        sendButton.setBounds(805, 80, 115, 55);

                        //---- attachmentButton ----
                        attachmentButton.setText("Attach");
                        CompseDetailsPane.add(attachmentButton);
                        attachmentButton.setBounds(805, 5, 115, 60);

                        { // compute preferred size
                            Dimension preferredSize = new Dimension();
                            for(int i = 0; i < CompseDetailsPane.getComponentCount(); i++) {
                                Rectangle bounds = CompseDetailsPane.getComponent(i).getBounds();
                                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                            }
                            Insets insets = CompseDetailsPane.getInsets();
                            preferredSize.width += insets.right;
                            preferredSize.height += insets.bottom;
                            CompseDetailsPane.setMinimumSize(preferredSize);
                            CompseDetailsPane.setPreferredSize(preferredSize);
                        }
                    }
                    NewEmailPane.add(CompseDetailsPane, BorderLayout.NORTH);

                    //======== bodyScroll ========
                    {
                        bodyScroll.setViewportView(messageBody);
                    }
                    NewEmailPane.add(bodyScroll, BorderLayout.CENTER);
                }
                SendView.add(NewEmailPane);
            }
            OptionsPane.addTab("Send New", SendView);

            //======== SearchView ========
            {
                SearchView.setLayout(new BorderLayout());

                //======== panel1 ========
                {
                    panel1.setLayout(null);

                    //======== scrollPane1 ========
                    {

                        //---- searchArea ----
                        searchArea.setWrapStyleWord(true);
                        searchArea.setLineWrap(true);
                        scrollPane1.setViewportView(searchArea);
                    }
                    panel1.add(scrollPane1);
                    scrollPane1.setBounds(20, 5, 765, 25);

                    //---- searchButton ----
                    searchButton.setText("Search");
                    panel1.add(searchButton);
                    searchButton.setBounds(795, 0, 110, searchButton.getPreferredSize().height);

                    { // compute preferred size
                        Dimension preferredSize = new Dimension();
                        for(int i = 0; i < panel1.getComponentCount(); i++) {
                            Rectangle bounds = panel1.getComponent(i).getBounds();
                            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                        }
                        Insets insets = panel1.getInsets();
                        preferredSize.width += insets.right;
                        preferredSize.height += insets.bottom;
                        panel1.setMinimumSize(preferredSize);
                        panel1.setPreferredSize(preferredSize);
                    }
                }
                SearchView.add(panel1, BorderLayout.NORTH);

                //======== scrollPane2 ========
                {
                    scrollPane2.setViewportView(resultMsgSubjects);
                }
                SearchView.add(scrollPane2, BorderLayout.WEST);

                //======== scrollPane3 ========
                {

                    //---- resultMsgBody ----
                    resultMsgBody.setLineWrap(true);
                    resultMsgBody.setWrapStyleWord(true);
                    resultMsgBody.setEditable(false);
                    scrollPane3.setViewportView(resultMsgBody);
                }
                SearchView.add(scrollPane3, BorderLayout.CENTER);
            }
            OptionsPane.addTab("Search", SearchView);
        }
        contentPane.add(OptionsPane);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Saifullah Azmi
    private JTabbedPane OptionsPane;
    private JPanel InboxView;
    private JScrollPane subjectScroll;
    private JList emailList;
    private JPanel MessagePanel;
    private JPanel MessaageDetails;
    private JLabel fromLabel;
    private JTextPane fromEmailAddress;
    private JLabel dateLabel;
    private JTextPane messageDate;
    private JLabel tagsLabel;
    private JTextPane tags;
    private JScrollPane messageScroll;
    private JTextArea emailMessage;
    private JPanel SendView;
    private JPanel NewEmailPane;
    private JPanel CompseDetailsPane;
    private JLabel toLabel;
    private JScrollPane toAddScroll;
    private JTextArea toAddresses;
    private JLabel ccLabel;
    private JScrollPane ccAddScroll;
    private JTextArea ccAddresses;
    private JLabel subjectLabel;
    private JScrollPane newSubjectScroll;
    private JTextArea newSubject;
    private JButton sendButton;
    private JButton attachmentButton;
    private JScrollPane bodyScroll;
    private JEditorPane messageBody;
    private JPanel SearchView;
    private JPanel panel1;
    private JScrollPane scrollPane1;
    private JTextArea searchArea;
    private JButton searchButton;
    private JScrollPane scrollPane2;
    private JList resultMsgSubjects;
    private JScrollPane scrollPane3;
    private JTextArea resultMsgBody;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
