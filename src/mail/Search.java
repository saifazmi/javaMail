package mail;

import com.sun.mail.imap.IMAPFolder;
import resources.ClientProperties;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Store;
import javax.mail.search.SearchTerm;
import javax.swing.DefaultListModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import static mail.sessions.IMAPSession.getIMAPSession;

/**
 * @author : saif
 * @project : javaMail
 * @date : 12/11/15
 */

/**
 * Represents a folder being searched
 */
public class Search {

    private static final Logger log = Logger.getLogger(Search.class.getName());
    private Store store;
    private IMAPFolder inboxFolder;
    private DefaultListModel<String> subjectList;
    private ArrayList<String> messageList;

    public Search() {

    }

    /**
     * Performs a search on a folder
     * @param query keyword to perform search with
     */
    public void searchFolder(final String query) {


        try {
            ClientProperties mailProperties = new ClientProperties();
            store = getIMAPSession().getStore("imaps");
            store.connect("imap.googlemail.com", mailProperties.getUsername(), mailProperties.getPassword());

            inboxFolder = (IMAPFolder) store.getFolder("inbox");

            inboxFolder.open(Folder.READ_ONLY);

            // Constructs a search term to apply on the folder
            SearchTerm searchCondition = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {
                        if (message.getSubject().toLowerCase().contains(query)) {
                            return true;
                        } else if (message.getContentType().contains("TEXT/PLAIN")) {
                            if (message.getContent().toString().toLowerCase().contains(query)) {
                                return true;
                            }
                        } else {
                            Multipart multipart = (Multipart) message.getContent();
                            for (int x = 0; x < multipart.getCount(); x++) {
                                BodyPart bodyPart = multipart.getBodyPart(x);
                                if (bodyPart.getContentType().contains("TEXT/PLAIN")) {
                                    if (bodyPart.getContent().toString().toLowerCase().contains(query)) {
                                        return true;
                                    }
                                }
                            }
                        }
                    } catch (MessagingException ex) {
                        log.log(Level.SEVERE, ex.toString(), ex);
                        ex.printStackTrace();
                    } catch (IOException e) {
                        log.log(Level.SEVERE, e.toString(), e);
                        e.printStackTrace();
                    }
                    return false;
                }
            };

            // Runs the search and stores the messages
            Message[] messagesFound = inboxFolder.search(searchCondition);

            int index = 0;
            this.subjectList = new DefaultListModel<>();
            this.messageList = new ArrayList<>();
            for (Message message : messagesFound) { // stores the relevant message parts in a list

                // only getting the plain text part of the email
                if (message.getContentType().contains("TEXT/PLAIN")) {
                    subjectList.add(index, message.getSubject()); // adding subjects to proper list model for GUI
                    messageList.add(index, message.getContent().toString()); // adding email body to list for GUI
                    log.log(Level.INFO, "Message found = " + index);
                } else {
                    // Getting parts from multiple body parts of MIME message
                    subjectList.add(index, message.getSubject());

                    Multipart multipart = (Multipart) message.getContent();
                    for (int x = 0; x < multipart.getCount(); x++) {
                        BodyPart bodyPart = multipart.getBodyPart(x);
                        if (bodyPart.getContentType().contains("TEXT/PLAIN")) {
                            messageList.add(index, bodyPart.getContent().toString());
                            log.log(Level.INFO, "Message found = " + index);
                        }
                    }
                }
                index++; // incrementing the index to keep all list aligned.
            }

        } catch (NoSuchProviderException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        } catch (MessagingException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        } catch (IOException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        } finally {
            if (inboxFolder != null && inboxFolder.isOpen()) {
                try {
                    inboxFolder.close(true);
                } catch (MessagingException e) {
                    log.log(Level.SEVERE, e.toString(), e);
                    e.printStackTrace();
                }
            }
            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException e) {
                    log.log(Level.SEVERE, e.toString(), e);
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Gets a list which contains the subjects for the searched messages in the folder.
     *
     * @return a list model containing subjects of messages.
     */
    public DefaultListModel<String> getSubjectList() {
        return this.subjectList;
    }

    /**
     * Gets a list which contains the searched message bodies.
     *
     * @return a list containing message bodies.
     */
    public ArrayList<String> getMessageList() {

        return this.messageList;
    }
}
