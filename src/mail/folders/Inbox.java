package mail.folders;

import com.sun.mail.imap.IMAPFolder;
import resources.ClientProperties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.swing.DefaultListModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import static mail.sessions.IMAPSession.getIMAPSession;

/**
 * @author : saif
 * @project : javaMail
 * @date : 11/11/15
 */

/**
 * Represents and stores the state of an Inbox
 */
public class Inbox {

    private static final Logger log = Logger.getLogger(Inbox.class.getName());

    private Store store;
    private IMAPFolder inboxFolder;
    private Message[] messages;
    private DefaultListModel<String> subjectList;
    private ArrayList<String> messageList;
    private ArrayList<String> msgTag;

    /**
     * Constructs the inbox with required data structures;
     */
    public Inbox() {

    }

    /**
     * Establishes a connection and assigns value to required fields.
     */
    public void open() {

        try {
            ClientProperties mailProperties = new ClientProperties();
            store = getIMAPSession().getStore("imaps");
            store.connect("imap.googlemail.com", mailProperties.getUsername(), mailProperties.getPassword());

            inboxFolder = (IMAPFolder) store.getFolder("inbox");

            if (!inboxFolder.isOpen())
                inboxFolder.open(Folder.READ_WRITE);

            messages = inboxFolder.getMessages();

            int index = 0;
            subjectList = new DefaultListModel<>();
            messageList = new ArrayList<>();
            msgTag = new ArrayList<>();

            for (Message message : messages) {

                // only getting the plain text part of the email
                if (message.getContentType().contains("TEXT/PLAIN")) {
                    subjectList.add(index, message.getSubject()); // adding subjects to proper list model for GUI
                    messageList.add(index, message.getContent().toString()); // adding email body to list for GUI
                    if (message.isSet(Flags.Flag.SEEN)) { // checks if the message has already been read
                        msgTag.add(index, "Read");  // assigns the appropriate tag
                    } else {
                        msgTag.add(index, "Un-Read");
                    }
                    log.log(Level.INFO, index + 1 + " message fetched");
                } else {
                    // Getting parts from multiple body parts of MIME message
                    subjectList.add(index, message.getSubject());
                    if (message.isSet(Flags.Flag.SEEN)) {
                        msgTag.add(index, "Read");
                    } else {
                        msgTag.add(index, "Un-Read");
                    }
                    Multipart multipart = (Multipart) message.getContent();
                    for (int x = 0; x < multipart.getCount(); x++) {
                        BodyPart bodyPart = multipart.getBodyPart(x);
                        if (bodyPart.getContentType().contains("TEXT/PLAIN")) {
                            messageList.add(index, bodyPart.getContent().toString());
                            log.log(Level.INFO, index + 1 + " message fetched");
                        }
                    }
                }
                index++; // incrementing the index to keep all list aligned.
            }
        } catch (NoSuchProviderException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }
    }

    /**
     * Closes all connections and folders.
     */
    public void close() {
        if (inboxFolder != null && inboxFolder.isOpen()) {
            try {
                inboxFolder.close(true);
            } catch (MessagingException e) {
                log.log(Level.SEVERE, e.toString(), e);
                e.printStackTrace();
            }
        }
        if (store != null) {
            try {
                store.close();
            } catch (MessagingException e) {
                log.log(Level.SEVERE, e.toString(), e);
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets a list which contains the subjects for all the messages in the folder.
     * @return a list model containing subjects of messages.
     */
    public DefaultListModel<String> getSubjectList() {
        return this.subjectList;
    }

    /**
     * Gets a list which contains the message bodies.
     * @return a list containing message bodies.
     */
    public ArrayList<String> getMessageList() {

        return this.messageList;
    }

    /**
     * Gets the email of the sender for a message
     * @param index the index value of the message
     * @return email address of the sender
     */
    public String getMessageFrom(int index) {

        String fromEmail = null;
        try {
            Address[] froms = messages[index].getFrom();
            fromEmail = froms == null ? null : ((InternetAddress) froms[0]).getAddress();
        } catch (MessagingException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return fromEmail;
    }

    /**
     * Gets the date on which a message was received.
     * @param index the index value of the message
     * @return date of the received message.
     */
    public String getMessageReceiveDate(int index) {

        String date = null;

        try {
            date = this.messages[index].getReceivedDate().toString();
        } catch (MessagingException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return date;
    }

    /**
     * Sets the value of flag for a message.
     * @param index the index value of the message.
     * @param flag the flag value to be set.
     * @param state the state of the flag being set.
     */
    public void setMessageFlags(int index, Flags.Flag flag, boolean state) {

        try {
            this.messages[index].setFlag(flag, state);
        } catch (MessagingException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }
    }

    /**
     * Gets the appropriate tags for a message.
     * @param index the index value of a message.
     * @return the tag for the message.
     */
    public String getMessageTags(int index) {

        return this.msgTag.get(index);
    }
}
