package mail.sessions;

import resources.ClientProperties;

import javax.mail.Session;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaMail
 * @date : 12/11/15
 */
public final class SMTPSession {

    private static final Logger log = Logger.getLogger(SMTPSession.class.getName());

    private SMTPSession() {

    }

    /**
     * Creates an SMTP session for.
     * @return an SMTP session.
     */
    public static Session getSMTPSession() {

        Properties connectionProperties = System.getProperties();
        ClientProperties mailProperties = new ClientProperties();

        connectionProperties.put("mail.smtp.auth", "true");
        connectionProperties.put("mail.smtp.starttls.enable", "true");
        connectionProperties.put("mail.smtp.host", mailProperties.getSmtpHost());
        connectionProperties.put("mail.smtp.port", "587");

        connectionProperties.setProperty("mail.user", mailProperties.getUsername());
        connectionProperties.setProperty("mail.password", mailProperties.getPassword());

        //Step 2: Establish a mail session (java.mail.Session)
        Session smtpSession = Session.getDefaultInstance(connectionProperties);

        return smtpSession;
    }
}
