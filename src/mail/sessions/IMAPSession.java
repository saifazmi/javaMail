package mail.sessions;

import resources.ClientProperties;

import javax.mail.Session;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaMail
 * @date : 11/11/15
 */
public final class IMAPSession {

    private static final Logger log = Logger.getLogger(IMAPSession.class.getName());

    private IMAPSession() {
    }

    /**
     * Creates an IMAP session for.
     * @return an IMAP session.
     */
    public static Session getIMAPSession() {
        Properties connectionProperties = System.getProperties();
        ClientProperties mailProperties = new ClientProperties();

        connectionProperties.setProperty("mail.store.protocol", mailProperties.getStoreProtocol());
        connectionProperties.setProperty("mail.user", mailProperties.getUsername());
        connectionProperties.setProperty("mail.password", mailProperties.getPassword());

        Session imapSession = Session.getDefaultInstance(connectionProperties);

        return imapSession;
    }
}
