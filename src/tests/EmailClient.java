package tests;

import GUI.EmailClientGUI;

import javax.swing.JFrame;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : javaMail
 * @date : 11/11/15
 */
public class EmailClient {

    private static final Logger log = Logger.getLogger(EmailClient.class.getName());

    public static void main(String[] args) {

        EmailClientGUI gui = new EmailClientGUI();
        gui.setSize(1200, 600);
        gui.setVisible(true);
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
